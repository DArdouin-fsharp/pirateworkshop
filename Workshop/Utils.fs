module Utils

open PirateType
open System
open System.Net.Http
open Newtonsoft.Json

let nobody : Pirate =
    { Name = "JohnDoe"; Handicap = Nothing; Boat = "None" ; Wanted = false; IsDrunk = false}

let random = Random()

let pirateBatch1 : Pirate list = [
    { nobody with Name = "Sandhurst 'Swab' Allen"; Boat = "Flying Dutchman" ; Handicap = WoodenLeg }
    { nobody with Name = "Cutler 'Speechless' Swien"; Boat = "Black Pearl" }
    { nobody with Name = "Charles 'The Coward' Clapham"; Boat = "Queen Anne's Revenge" }
    { nobody with Name = "Faxon 'The Cold' Branson"; Boat = "Royal Fortune" ; Handicap = Eyepatch }
    { nobody with Name = "Nickleby 'The Sour' Bentley"; Boat = "Queen Anne's Revenge" }
    { nobody with Name = "Patton 'Howler' Chrom"; Boat = "Jackdaw" }
    { nobody with Name = "Chetwin 'Phantasm' Clare"; Boat = "Jackdaw" ; Handicap = Hook }
    { nobody with Name = "Hunter 'The Ghost' Keats"; Boat = "Flying Dutchman" }
    { nobody with Name = "Stanmore 'Snitch' Pritchard"; Boat = "Queen Anne's Revenge" ; Handicap = WoodenLeg }
    { nobody with Name = "Bourne 'The Legend' Whulsup"; Boat = "Queen Anne's Revenge" }
    { nobody with Name = "Harper 'Crow's Nest' Rodney"; Boat = "Jackdaw" }
    { nobody with Name = "Barbara 'Haunted' Quye"; Boat = "Black Pearl" ; Handicap = WoodenLeg }
    { nobody with Name = "Antonia 'The Shadow' Brown"; Boat = "Royal Fortune" }
    { nobody with Name = "Dita 'Twisting' Watt"; Boat = "Royal Fortune" }
    { nobody with Name = "Angeline 'The Hero' Asheton"; Boat = "Jackdaw" ; Handicap = Eyepatch }
    { nobody with Name = "Shelley 'Bashed' Clemons"; Boat = "Flying Dutchman" }
    { nobody with Name = "Vina 'Crippled' Hitch"; Boat = "Black Pearl" ; Handicap = Hook }
]

let pirateBatch2 : Pirate list = [
    { nobody with Name = "Retta 'Rough Dog' Asema"; Boat = "Queen Anne's Revenge" }
    { nobody with Name = "Nita 'One-Eared' Ross"; Boat = "Jackdaw" }
    { nobody with Name = "Bernadine 'Two-Teeth' Brandon"; Boat = "Black Pearl" ; Handicap = WoodenLeg }
    { nobody with Name = "Wilford 'Savage Soul' Dukes"; Boat = "Jackdaw" }
    { nobody with Name = "Westbrook 'Mutiny' Kellam"; Boat = "Queen Anne's Revenge" }
    { nobody with Name = "Udolf 'Foxy' Browning"; Boat = "Royal Fortune" ; Handicap = Eyepatch }
    { nobody with Name = "Northrop 'Treason' Bryce"; Boat = "Flying Dutchman" ; Handicap = WoodenLeg }
    { nobody with Name = "Yorick 'Tormenting' Prince"; Boat = "Black Pearl" }
    { nobody with Name = "Kenelm 'Handsome' Rakshasas"; Boat = "Queen Anne's Revenge" }
    { nobody with Name = "Kelsey 'No-Fingers' Hornsby"; Boat = "Flying Dutchman" ; Handicap = Eyepatch }
    { nobody with Name = "Nyle 'Intrepid' Joshua"; Boat = "Royal Fortune" ; Wanted = true }
    { nobody with Name = "Garroway 'Black Eyes' Hogan"; Boat = "Queen Anne's Revenge" }
    { nobody with Name = "Maymie 'The Shadow' Glacier"; Boat = "Jackdaw" }
    { nobody with Name = "Annabell 'Phantasm' Swales"; Boat = "Flying Dutchman" ; Handicap = Hook }
    { nobody with Name = "Josefa 'Fierceful' Atherton"; Boat = "Royal Fortune" ; Handicap = WoodenLeg }
    { nobody with Name = "Olevia 'The Handsome' Bonney"; Boat = "Black Pearl" }
    { nobody with Name = "Mary 'Reaper' Lynx"; Boat = "Flying Dutchman" }
    { nobody with Name = "Merle 'Riot' Cheek"; Boat = "Black Pearl" }
    { nobody with Name = "Elfrida 'One-Tooth' Alby"; Boat = "Black Pearl" ; Handicap = Eyepatch }
    { nobody with Name = "Elvera 'Jagged' Derane"; Boat = "Queen Anne's Revenge" ; Handicap = Hook }
    { nobody with Name = "Callie 'Lost Soul' Amaranth"; Boat = "Queen Anne's Revenge" }
]

let drink (p : Pirate) : Pirate =
    { p with IsDrunk = true; }

let fightWithVanDerDecken (p : Pirate) : Pirate =
    { p with Handicap = Eyepatch; }

let beingRecruited (p : Pirate) : Pirate = 
    { p with Boat = "Flying Dutchman"; }

let plunder (p : Pirate) : Pirate = 
    { p with Wanted = true; }


type ExerciceRequest = {
    Name : string;
    Handicap : string;
    Boat : string;
}
type ExerciceResponse = {
    Key: string
}

let submit (p : Pirate) : Option<string> = 
    use httpClient = new HttpClient()
    httpClient.BaseAddress <- Uri("https://workshop-validator20190913064025.azurewebsites.net/")
    let requestJSON = JsonConvert.SerializeObject({Name = p.Name; Handicap = p.Handicap.ToString() ; Boat = p.Boat ; })
    let response = httpClient.PostAsync("api/validation", new StringContent(requestJSON, Text.Encoding.UTF8, "application/json")) |> Async.AwaitTask  |> Async.RunSynchronously
    let responseJSON = response.Content.ReadAsStringAsync() |> Async.AwaitTask |> Async.RunSynchronously
    let response = JsonConvert.DeserializeObject<ExerciceResponse>(responseJSON).Key

    if String.IsNullOrEmpty(response) then None else Some(response)

let trySubmitPirate (errorMessage : string) (p : Pirate) : unit =
    match submit p with 
    | Some s -> printf "Congratulation, here's your key : %s \n\n" s
    | None _ -> printf "%s \n\n" errorMessage