module Workshop

open PirateType
open System
open System.Net.Http
open Utils

//Before each exercice, don't forget to uncomment it
//You need to uncomment the corresponding line in the main method aswell
// Ctrl + K + U to uncomment your selection :)
//
//To test your exercices, from the embedded terminal :
//     cd Workshop
//     ./buildAndRun.cmd
//This can be found under folder

//-------------------------------------------------------
//-------------------------------------------------------
//Exercice 1 : Types
//First create missing types in PirateType.fs
//Then, implement the creation method

let createPirate : Pirate =
    {Name = "Jack Sparrow"; Handicap = Nothing; Boat = "Black Pearl"; Wanted = true; IsDrunk = true}

//-------------------------------------------------------
let exercice1 : unit =
    printfn "Exercice 1 : \n"
    createPirate
    |> trySubmitPirate "We are looking for the most famous pirate of this century. You can find him in a film serie. The actor's name is Johnny..."

// //-------------------------------------------------------
// //-------------------------------------------------------
// //Exercice 2 : Pattern matching and option type
// //You need to implement a function that will help you to find the pirate of interest
// //You can do a pattern match on the handicap. Then filter on the boat...

let isPirateOfInterest (pirate : Pirate) : Option<Pirate> = 
    match pirate.Boat, pirate.Handicap with 
    | "Queen Anne's Revenge", WoodenLeg -> Some pirate
    | _, _ -> None

//-------------------------------------------------------
let rec loop (acc : int) : unit =
    if acc < 500 then
        let i = random.Next(0, 17)
        match pirateBatch1.Item i |> isPirateOfInterest with
        | Some p -> p |> trySubmitPirate "We are looking for a crew member of Edward Teach. This guy might not be able to run as fast as when he was a kid..."
        | None _ -> loop (acc + 1)
    else printf "You did not submit any pirate... \n\nWe are looking for a crew member of Captain Edward Teach. This guy might not be able to run as fast as when he was a kid..."

let exercice2 : unit = 
    printfn "Exercice 2 : \n"
    loop 0

// //-------------------------------------------------------
// //-------------------------------------------------------
// //Exercice 3 : Recursion
// //Using recursion, find the wanted pirate in 'pirates' list
// //Don't forget to throw an exception if you reach the end of the list !

let rec analysePirates pirates =  
    match pirates with
    | [] -> failwithf "No more pirates :("
    | head::_ when head.Wanted -> head 
    | _::tail -> analysePirates tail

//-------------------------------------------------------
let exercice3 : unit = 
    printfn "Exercice 3 : \n"
    pirateBatch2
    |> analysePirates
    |> trySubmitPirate "We are looking for the one and only wanted pirate"

// //-------------------------------------------------------
// //-------------------------------------------------------
// //Exercice 4 : Function composition
// //You need to create a function 'scenario' of type Pirate -> Pirate
// //The following scenario is typical of a pirate's life :
// //
// //With the money from his last adventure, your pirate will get drunk 
// //Because he can't hold his liquor, he will try to fight with other pirates
// //If he wins the fight he might be recruited !
// //And then leave for his new adventures
// //(Those methods are already implemented : drink, fightWithVanDerDecken, beingRecruited, plunder)

let scenario : Pirate -> Pirate = 
    drink
    >> fightWithVanDerDecken
    >> beingRecruited
    >> plunder

//-------------------------------------------------------
let exercice4 : unit = 
    printfn "Exercice 4 : \n"
    nobody
    |> scenario
    |> trySubmitPirate "The pirate's life was much more agitated than that"

// //-------------------------------------------------------
// //-------------------------------------------------------
// //Exercice 5 : Web call
// //Make a call to this rest api https://workshop-validator20190913064025.azurewebsites.net with this route api/get21thCenturyPirate
// //No payload is needed
// //The answer will be the name of the pirate we are looking for
// //Return a copy of 'nobody', where the name has been changed for the one returned by the web call

let get21thCenturyPirate : Pirate = 
    use client = new HttpClient()
    async {
         use content = new StringContent("")
         let! x = client.PostAsync("https://workshop-validator20190913064025.azurewebsites.net/api/get21thCenturyPirate", content) |> Async.AwaitTask
         let! pirateName = x.Content.ReadAsStringAsync() |> Async.AwaitTask
         return {nobody with Name = pirateName}
    } |> Async.RunSynchronously

//-------------------------------------------------------
let exercice5 : unit = 
    printfn "Exercice 5 : \n"
    let x = get21thCenturyPirate
    x |> trySubmitPirate "teet"

[<EntryPoint>]
let main args =
    exercice1
    exercice2
    exercice3
    exercice4
    exercice5
    0