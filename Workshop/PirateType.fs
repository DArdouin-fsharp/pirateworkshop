module PirateType

// We expect you to create a record type "Pirate", and a union type "Handicap".
// Handicap list : Nothing, Hook, WoodenLeg, Eyepatch
// Your pirate will have :
// Name (string), Handicap (Handicap), Boat (string), Wanted (bool), IsDrunk (bool)

type Handicap =  
    | Nothing
    | WoodenLeg
    | Hook
    | Eyepatch

type Pirate =
 { Name : string; Handicap : Handicap ; Boat : string; Wanted : bool; IsDrunk : bool }